#ifndef __GRAPH__
#define __GRAPH__
#define MAX 10000

typedef struct graph {
    int n;
    int **a;
}graph;

typedef struct edge {
    int u, v;
}edge;

typedef struct result {
    int *parent;
    int *distance;
}result;

void initGraph(graph *g, int n);
graph *createGraphFromMap(char *filename);
int search(char *citynames[], char *name, int n);
void insertEdge(graph *g, int i, int j, int weight);


void initGraph(graph *g, int n);
void insertEdge(graph *g, int i, int j, int weight);
int readline(FILE *fp, char *line, int n);
int search(char *citynames[], char *name, int n);
graph *createGraphFromFile(char *filename);
result bellman_ford(graph g, int start);
void printpath(int *parent, int n, int start);
void printPaths(graph g);






#endif